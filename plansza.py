# -*- coding: utf-8 -*-
# Form implementation generated from reading ui file 'plansza.ui'
#
# Created by: PyQt5 UI code generator 5.7
#
# WARNING! All changes made in this file will be lost!
from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.QtGui import *

class Ui_Form(object):
    def setupUi(self, Form):
        self.karta = [0,0,0,0,0,0,0,0,0,0,0,0]
        Form.setObjectName("Form")
        Form.resize(1000, 1000)
        self.gridLayoutWidget = QtWidgets.QWidget(Form)
        self.gridLayoutWidget.setGeometry(QtCore.QRect(-10, 30, 1000, 800))
        self.gridLayoutWidget.setObjectName("gridLayoutWidget")
        self.gridLayout = QtWidgets.QGridLayout(self.gridLayoutWidget)
        self.gridLayout.setContentsMargins(0, 0, 0, 0)
        self.gridLayout.setObjectName("gridLayout")
        self.karta[11] = QtWidgets.QPushButton(self.gridLayoutWidget)
        self.karta[11].setCursor(QtGui.QCursor(QtCore.Qt.OpenHandCursor))
        self.karta[11].setFocusPolicy(QtCore.Qt.NoFocus)
        self.karta[11].setStyleSheet("border: none;\n" "text-align: center;\n"
"")
        self.karta[11].setText("")
        icon = QtGui.QIcon()
        icon.addPixmap(QtGui.QPixmap("img/karta.png"), QtGui.QIcon.Normal)        
        self.karta[11].setIcon(icon)
        self.karta[11].setObjectName("karta[11]")
        self.gridLayout.addWidget(self.karta[11], 2, 4, 1, 1)
        self.karta[6] = QtWidgets.QPushButton(self.gridLayoutWidget)
        self.karta[6].setCursor(QtGui.QCursor(QtCore.Qt.OpenHandCursor))
        self.karta[6].setFocusPolicy(QtCore.Qt.NoFocus)
        self.karta[6].setStyleSheet("border: none;\n"
"text-align: center;\n")
        self.karta[6].setText("")
        self.karta[6].setIcon(icon)
        self.karta[6].setObjectName("karta[6]")
        self.gridLayout.addWidget(self.karta[6], 1, 1, 1, 1)
        self.karta[3] = QtWidgets.QPushButton(self.gridLayoutWidget)
        self.karta[3].setCursor(QtGui.QCursor(QtCore.Qt.OpenHandCursor))
        self.karta[3].setFocusPolicy(QtCore.Qt.NoFocus)
        self.karta[3].setStyleSheet("border: none;\n" 
"text-align: center;\n"
"")
        self.karta[3].setText("")
        self.karta[3].setIcon(icon)
        self.karta[3].setObjectName("karta[3]")
        self.gridLayout.addWidget(self.karta[3], 0, 1, 1, 1)
        self.karta[4] = QtWidgets.QPushButton(self.gridLayoutWidget)
        self.karta[4].setCursor(QtGui.QCursor(QtCore.Qt.OpenHandCursor))
        self.karta[4].setFocusPolicy(QtCore.Qt.NoFocus)
        self.karta[4].setStyleSheet("border: none;\n"
 
"text-align: center;\n"
"")
        self.karta[4].setText("")
        self.karta[4].setIcon(icon)
        self.karta[4].setObjectName("karta[4]")
        self.gridLayout.addWidget(self.karta[4], 0, 2, 1, 1)
        self.karta[0] = QtWidgets.QPushButton(self.gridLayoutWidget)
        self.karta[0].setCursor(QtGui.QCursor(QtCore.Qt.OpenHandCursor))
        self.karta[0].setFocusPolicy(QtCore.Qt.NoFocus)
        self.karta[0].setStyleSheet("border: none;\n"
 
 
"text-align: center;\n"
"")
        self.karta[0].setText("")
        self.karta[0].setIcon(icon)
        self.karta[0].setObjectName("karta[0]")
        self.gridLayout.addWidget(self.karta[0], 0, 0, 1, 1)
        self.karta[2] = QtWidgets.QPushButton(self.gridLayoutWidget)
        self.karta[2].setCursor(QtGui.QCursor(QtCore.Qt.OpenHandCursor))
        self.karta[2].setFocusPolicy(QtCore.Qt.NoFocus)
        self.karta[2].setStyleSheet("border: none;\n"
 
 
"text-align: center;\n"
"")
        self.karta[2].setText("")
        self.karta[2].setIcon(icon)
        self.karta[2].setObjectName("karta[2]")
        self.gridLayout.addWidget(self.karta[2], 2, 0, 1, 1)
        self.karta[9] = QtWidgets.QPushButton(self.gridLayoutWidget)
        self.karta[9].setCursor(QtGui.QCursor(QtCore.Qt.OpenHandCursor))
        self.karta[9].setFocusPolicy(QtCore.Qt.NoFocus)
        self.karta[9].setStyleSheet("border: none;\n"
 
"text-align: center;\n"
"")
        self.karta[9].setText("")
        self.karta[9].setIcon(icon)
        self.karta[9].setObjectName("karta[9]")
        self.gridLayout.addWidget(self.karta[9], 2, 1, 1, 1)
        self.karta[5] = QtWidgets.QPushButton(self.gridLayoutWidget)
        self.karta[5].setCursor(QtGui.QCursor(QtCore.Qt.OpenHandCursor))
        self.karta[5].setFocusPolicy(QtCore.Qt.NoFocus)
        self.karta[5].setStyleSheet("border: none;\n"
 
 
"text-align: center;\n"
"")
        self.karta[5].setText("")
        self.karta[5].setIcon(icon)
        self.karta[5].setObjectName("karta[5]")
        self.gridLayout.addWidget(self.karta[5], 0, 4, 1, 1)
        self.karta[8] = QtWidgets.QPushButton(self.gridLayoutWidget)
        self.karta[8].setCursor(QtGui.QCursor(QtCore.Qt.OpenHandCursor))
        self.karta[8].setFocusPolicy(QtCore.Qt.NoFocus)
        self.karta[8].setStyleSheet("border: none;\n"
 
 
"text-align: center;\n"
"")
        self.karta[8].setText("")
        self.karta[8].setIcon(icon)
        self.karta[8].setObjectName("karta[8]")
        self.gridLayout.addWidget(self.karta[8], 1, 4, 1, 1)
        self.karta[10] = QtWidgets.QPushButton(self.gridLayoutWidget)
        self.karta[10].setCursor(QtGui.QCursor(QtCore.Qt.OpenHandCursor))
        self.karta[10].setFocusPolicy(QtCore.Qt.NoFocus)
        self.karta[10].setStyleSheet("border: none;\n"
 
"text-align: center;\n"
"")
        self.karta[10].setText("")
        self.karta[10].setIcon(icon)
        self.karta[10].setObjectName("karta[10]")
        self.gridLayout.addWidget(self.karta[10], 2, 2, 1, 1)
        self.karta[7] = QtWidgets.QPushButton(self.gridLayoutWidget)
        self.karta[7].setCursor(QtGui.QCursor(QtCore.Qt.OpenHandCursor))
        self.karta[7].setFocusPolicy(QtCore.Qt.NoFocus)
        self.karta[7].setStyleSheet("border: none;\n"
"text-align: center;\n"
"")
        self.karta[7].setText("")
        self.karta[7].setIcon(icon)
        self.karta[7].setObjectName("karta[7]")
        self.gridLayout.addWidget(self.karta[7], 1, 2, 1, 1)
        self.karta[1] = QtWidgets.QPushButton(self.gridLayoutWidget)
        self.karta[1].setCursor(QtGui.QCursor(QtCore.Qt.OpenHandCursor))
        self.karta[1].setFocusPolicy(QtCore.Qt.NoFocus)
        self.karta[1].setStyleSheet("border: none;\n"
 
"text-align: center;\n"
"")
        self.karta[1].setText("")
        self.karta[1].setIcon(icon)
        self.karta[1].setObjectName("karta[1]")
        self.gridLayout.addWidget(self.karta[1], 1, 0, 1, 1)
        self.label_2 = QtWidgets.QLabel(self.gridLayoutWidget)
        self.label_2.setObjectName("label_2")
        self.gridLayout.addWidget(self.label_2, 3, 2, 1, 1)
        self.Wyniki = QtWidgets.QLabel(self.gridLayoutWidget)
        self.Wyniki.setObjectName("Wyniki")
        self.gridLayout.addWidget(self.Wyniki, 3, 4, 1, 1)
        self.label = QtWidgets.QLabel(Form)
        self.label.setGeometry(QtCore.QRect(0, 0, 361, 31))
        self.label.setStyleSheet("text-align: center;\n"
"")
        self.label.setObjectName("label")
        self.karta[11].setIconSize(QtCore.QSize(250,230))
        self.karta[10].setIconSize(QtCore.QSize(250,230))
        self.karta[9].setIconSize(QtCore.QSize(250,230))
        self.karta[8].setIconSize(QtCore.QSize(250,230))
        self.karta[7].setIconSize(QtCore.QSize(250,230))
        self.karta[6].setIconSize(QtCore.QSize(250,230))
        self.karta[5].setIconSize(QtCore.QSize(250,230))
        self.karta[4].setIconSize(QtCore.QSize(250,230))
        self.karta[3].setIconSize(QtCore.QSize(250,230))
        self.karta[2].setIconSize(QtCore.QSize(250,230))
        self.karta[1].setIconSize(QtCore.QSize(250,230))
        self.karta[0].setIconSize(QtCore.QSize(250,230))
        self.retranslateUi(Form)
        self.Wyniki.setStyleSheet("text-align: center; font-size: 72px;")
        self.label.setStyleSheet("text-align: center; font-size: 72px;")
        self.label_2.setStyleSheet("text-align: center; font-size: 72px;")
        QtCore.QMetaObject.connectSlotsByName(Form)

    def retranslateUi(self, Form):
        _translate = QtCore.QCoreApplication.translate
        Form.setWindowTitle(_translate("Form", "Form"))
        self.label_2.setText(_translate("Form", "Wynik"))
        self.Wyniki.setText(_translate("Form", "0"))
        self.label.setText(_translate("Form", "Odgadnji sześć takich samych kart"))

