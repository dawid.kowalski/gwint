import sys, time
from PyQt5 import QtCore, QtGui, QtWidgets, QtTest
from PyQt5.QtWidgets import QDialog, QApplication
from PyQt5.QtGui import *
from plansza import Ui_Form
from losuj import losuj
from functools import partial

class AppWindow(QDialog):
    widok = 0
    pierwszy = False
    wybor = ""
    idwyb = 0
    rund = 0
    odgadniete = 0
    def __init__(self):
        super().__init__()
        self.ui = Ui_Form()
        self.ui.setupUi(self)
        self.widok = losuj()
        self.show()  
        self.ui.karta[0].clicked.connect(partial(self.odkryj, 0))
        self.ui.karta[1].clicked.connect(partial(self.odkryj, 1))
        self.ui.karta[2].clicked.connect(partial(self.odkryj, 2))
        self.ui.karta[3].clicked.connect(partial(self.odkryj, 3))
        self.ui.karta[4].clicked.connect(partial(self.odkryj, 4))
        self.ui.karta[5].clicked.connect(partial(self.odkryj, 5))
        self.ui.karta[6].clicked.connect(partial(self.odkryj, 6))
        self.ui.karta[7].clicked.connect(partial(self.odkryj, 7))
        self.ui.karta[8].clicked.connect(partial(self.odkryj, 8))
        self.ui.karta[9].clicked.connect(partial(self.odkryj, 9))
        self.ui.karta[10].clicked.connect(partial(self.odkryj, 10))
        self.ui.karta[11].clicked.connect(partial(self.odkryj, 11))
    def odkryj(self, id_karty):
        icon = QtGui.QIcon()
        icon.addPixmap(QtGui.QPixmap(self.widok.grafika[self.widok.losowanie[id_karty]]), QtGui.QIcon.Normal)        
        self.ui.karta[id_karty].setIcon(icon)
        if self.pierwszy == False:
            self.pierwszy = True
            self.wybor = self.widok.grafika[self.widok.losowanie[id_karty]]
            self.idwyb = id_karty
            self.ui.karta[id_karty].disconnect()
            self.ui.karta[id_karty].setStyleSheet("QPushButton:disabled { background-color: black }")
        else:
            self.pierwszy = False
            self.sprawdz(self.wybor, self.widok.grafika[self.widok.losowanie[id_karty]], self.idwyb, id_karty)
    def sprawdz(self, a, b, ida, idb):
        if a != b:
            QtTest.QTest.qWait(500)
            self.ui.karta[ida].clicked.connect(partial(self.odkryj, ida))
            self.ui.karta[idb].clicked.connect(partial(self.odkryj, idb))
            icon = QtGui.QIcon()
            icon.addPixmap(QtGui.QPixmap("img/karta.png"), QtGui.QIcon.Normal)        
            self.ui.karta[ida].setIcon(icon)
            self.ui.karta[idb].setIcon(icon)
        else:
            self.ui.karta[ida].disconnect()
            self.ui.karta[idb].disconnect()
            icon = QtGui.QIcon()
            QtTest.QTest.qWait(500)
            icon.addPixmap(QtGui.QPixmap("img/trafiony.png"), QtGui.QIcon.Normal)        
            self.ui.karta[ida].setIcon(icon)
            self.ui.karta[idb].setIcon(icon)
            self.odgadniete += 1
            if
        self.rund = self.rund + 1
        self.ui.Wyniki.setText(str(self.rund))
app = QApplication(sys.argv)
w = AppWindow()
w.show()
sys.exit(app.exec_())
